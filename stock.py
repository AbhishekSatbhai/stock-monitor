import requests
from bs4 import BeautifulSoup
import time
import datetime
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

# setup
COIN = "dogecoin"
URL = "https://coinmarketcap.com/currencies"
ELEMENT_DIV_TO_FETCH_PATH = "div", {"class": "priceValue___11gHJ"}  # element class name
TIME_BETWEEN_CONSECUTIVE_CURRENCY_CHECK = 2  # in seconds
UPPER_LIMIT = 0.3300  # in dollars
LOWER_LIMIT = 0.3200  # in dollars


def buildURL(url, coin):
    URL = f"{url}/{coin}"
    return URL


def getData(URL):
    page = requests.get(URL)
    soup = BeautifulSoup(page.content, "html.parser")
    results = soup.find_all(ELEMENT_DIV_TO_FETCH_PATH)
    print(results[0].text)
    return results[0].text


def sendEmail(emailSubject, mail_content):

    # The mail addresses and password
    sender_address = "abhishek07satbhai@gmail.com"
    sender_pass = ""
    receiver_address = "abhisheksatbhai@icloud.com"

    # Setup the MIME
    message = MIMEMultipart()
    message["From"] = sender_address
    message["To"] = receiver_address
    message["Subject"] = emailSubject

    # The body and the attachments for the mail
    message.attach(MIMEText(mail_content, "html", "utf-8"))

    # Create SMTP session for sending the mail
    session = smtplib.SMTP("smtp.gmail.com", 587)
    session.starttls()
    session.login(sender_address, sender_pass)
    text = message.as_string()
    session.sendmail(sender_address, receiver_address, text)
    session.quit()
    log_content = (
        f"Date Time: {datetime.datetime.now()} Email Sent, Alerted about change in cost"
    )
    logs(log_content)


def logs(content):
    with open("logs.txt", "a") as logs:
        logs.write(content + "\n")


def buildEmailData(data, coin):
    mail_content = (
        f"""<h3>,<br> Hello,<br>Current Price for {coin} is : {data}/> <br>"""
    )


def checkThreshold(upperLimit, lowerLimit, coin):
    builtURL = buildURL(url=URL, coin=COIN)
    data = float(getData(builtURL)[2:])

    if data < lowerLimit:
        log_content = (
            f"Date Time: {datetime.datetime.now()}, Price Dropped below lowerlimit "
        )
        logs(log_content)
        emailSubject = f"{coin} value ${data} is less than lower limit ${lowerLimit}"
        mail_content = buildEmailData()
        sendEmail(emailSubject, mail_content)

    elif data > upperLimit:
        log_content = (
            f"Date Time: {datetime.datetime.now()}, Price Increased above upperlimit "
        )
        logs(log_content)
        emailSubject = f"{coin} value ${data} is more than upper limit ${upperLimit}"
        mail_content = buildEmailData(data, coin)
        sendEmail(emailSubject, mail_content)


while True:
    checkThreshold(upperLimit=UPPER_LIMIT, lowerLimit=LOWER_LIMIT, coin=COIN)
    print("checking..")
    time.sleep(TIME_BETWEEN_CONSECUTIVE_CURRENCY_CHECK)
