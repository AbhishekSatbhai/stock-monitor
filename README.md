# stock-monitor
This script monitors coins like dogecoin, by web scrapping dom of provided URL and Element path.
User need to set upperlimit and lowerlimit of coin and when rate of that particular coin will cross its limits (upper or lower). The user will be alerted throught email. Also there is a log maintained in txt file for reference.

P.S: There are lots of other scripts available but this was a quick script I could develope as per my friend Mukund's requirement :) so thanks to him.

# How to run

* Install all the packages listed in requirements.txt
  * CMD : pip install -r requirement.txt
* Setup 
    * Coins to fetch 
    * URL
    * class of element
    * Upper and lower Limit threshold
    * Time interval between two conecutive checks
* Set your email address and password, also receivers email address 
* Finally run the script 'python stock.py'
    * If you want to run in shell for mac use 'python net_speed_alert.py &'
    * you need zsh terminal to run above in shell